package com.agungfir.infinitescroll

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.agungfir.infinitescroll.adapter.MovieAdapter
import com.agungfir.infinitescroll.model.Movie
import com.agungfir.infinitescroll.model.MovieResponse
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson

class MovieActivity : AppCompatActivity() {

    private lateinit var swipeMain: SwipeRefreshLayout
    private lateinit var rvMovies: RecyclerView
    private val movieAdapter: MovieAdapter = MovieAdapter()
    private lateinit var requestQueue: RequestQueue
    private var page: Int = 1
    private var lastPage: Int = 0
    private var isScroll = true

    companion object {
        var TAG: String = MovieActivity::class.java.simpleName
    }

    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie)
        swipeMain = findViewById(R.id.swipeMain)
        rvMovies = findViewById(R.id.rvMovies)
        swipeMain.setColorSchemeColors(R.color.purple_200)

        fetchData(page)
        val linearLayoutManager = LinearLayoutManager(this)
        rvMovies.setHasFixedSize(true)
        rvMovies.layoutManager = linearLayoutManager
        rvMovies.adapter = movieAdapter

        rvMovies.addOnScrollListener(
            object : RecyclerView.OnScrollListener() {

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)

                    val countItems = linearLayoutManager.itemCount
                    val currentItems = linearLayoutManager.childCount
                    val firstVisiblePosition = linearLayoutManager.findFirstVisibleItemPosition()
                    val totalScrollItem = currentItems + firstVisiblePosition

                    if (isScroll && totalScrollItem >= countItems && page <= lastPage) {
                        isScroll = false
                        page += 1
                        fetchData(page)
                    }
                }
            }
        )

    }

    private fun fetchData(page: Int) {
        loadLoading()
        requestQueue = Volley.newRequestQueue(this)

        val stringRequest = StringRequest(
            Request.Method.GET,
            "${BuildConfig.BASE_URL_MOVIE_DB}${BuildConfig.API_VERSION}discover/movie?api_key=${BuildConfig.API_KEY}&page=$page",
            object : Response.Listener<String> {
                override fun onResponse(response: String?) {
                    hideLoading()
                    if (response != null) {
                        val movieResponse = Gson().fromJson(response, MovieResponse::class.java)
                        if (movieAdapter.getDateMovies() > 0) {
                            movieAdapter.removeDataLoading()
                            isScroll = true
                        }
                        lastPage = movieResponse.totalPages!!
                        movieAdapter.addDataMovies(movieResponse.results as MutableList<Movie>)
                    }
                }

            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError?) {
                    Log.e(TAG, error?.message.toString())
                }

            }
        )

        stringRequest.setTag(TAG)
        requestQueue.add(stringRequest)
    }

    private fun hideLoading() {
        swipeMain.isRefreshing = false
    }

    private fun loadLoading() {
        if (movieAdapter.getDateMovies() > 0) {
            movieAdapter.addDataLoading()
        }
        swipeMain.isRefreshing = true
    }

    override fun onStop() {
        super.onStop()
        // clear all request, karena jika tidak ketika pindah halaman maka akan error
        if (requestQueue != null) {
            requestQueue.cancelAll(TAG)
        }
    }
}