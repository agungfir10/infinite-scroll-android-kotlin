package com.agungfir.infinitescroll

import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.agungfir.infinitescroll.adapter.ContactAdapter


class MainActivity : AppCompatActivity() {

    private var isScroll = true
    private lateinit var contactAdapter: ContactAdapter
    private var dataContacts: MutableList<String> = mutableListOf()
    private lateinit var rvContacts: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        rvContacts = findViewById(R.id.rvContacts)


        contactAdapter = ContactAdapter()
        fetchData(0)
        val linearLayoutManager = LinearLayoutManager(this)
        rvContacts.apply {
            setHasFixedSize(true)
            adapter = contactAdapter
            layoutManager = linearLayoutManager
        }

        rvContacts.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                // jumlah data yang ada pada recyclerview
                val countItems = linearLayoutManager.itemCount
                // jumlah data yang terlihat
                val currentItems = linearLayoutManager.childCount
                // index data yang terlihat pertama kali (kalo vertical index data yang berada diatas)
                val firstVisiblePosition = linearLayoutManager.findFirstVisibleItemPosition()

                val totalScrollItem = currentItems + firstVisiblePosition

                Log.d("COBA", "count item $countItems")
                Log.d("COBA", "current item $currentItems")
                Log.d("COBA", "first visible pos $firstVisiblePosition")

                if (isScroll && totalScrollItem == countItems) {
                    isScroll = false
                    contactAdapter.addDataLoading()

                    Handler(mainLooper).postDelayed({
                        contactAdapter.removeDataLoading()
                        fetchData(countItems)
                        isScroll = true
                    }, 2000)
                }
            }
        })
    }

    private fun fetchData(countItems: Int) {
        if (dataContacts.size > 0) {
            dataContacts.clear()
        }
        val n = countItems + 15
        for (i in countItems..n) {
            dataContacts.add("$i John Doe")
        }
        contactAdapter.addDataContacts(dataContacts)
    }
}