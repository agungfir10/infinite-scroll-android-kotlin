package com.agungfir.infinitescroll.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.agungfir.infinitescroll.BuildConfig
import com.agungfir.infinitescroll.R
import com.agungfir.infinitescroll.model.Movie
import com.bumptech.glide.Glide

class MovieAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var dataMovies: MutableList<Movie?> = mutableListOf()

    companion object {
        private const val TYPE_MOVIE: Int = 1
        private const val TYPE_LOADING: Int = 2
    }

    class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTitleMovie: TextView = itemView.findViewById(R.id.tvTitleMovie)
        val ivPosterMovie: ImageView = itemView.findViewById(R.id.ivPoster)
        val tvReleaseDate: TextView = itemView.findViewById(R.id.tvReleaseDate)
        val tvRatingMovie: TextView = itemView.findViewById(R.id.tvRatingMovie)
        val ratingBarMovie: RatingBar = itemView.findViewById(R.id.ratingBarMovie)

        fun bind(movie: Movie?) {
            if (movie != null) {
                tvTitleMovie.text = movie?.title
                tvReleaseDate.text = "Release date : ${movie.releaseDate}"
                tvRatingMovie.text = (movie.voteAverage?.div(2)).toString()
                ratingBarMovie.rating = (movie.voteAverage?.div(2)!!)
                Glide
                    .with(itemView)
                    .load("${BuildConfig.BASE_URL_IMAGE_MOVIE_DB}${movie.posterPath}")
                    .into(ivPosterMovie)
            }
        }

    }

    class ProgressViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == TYPE_MOVIE) {
            return MovieViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
            )
        }
        return ProgressViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_loading, parent, false)
        )

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == TYPE_MOVIE) {
            val movieViewHolder: MovieViewHolder = holder as MovieViewHolder
            movieViewHolder.bind(dataMovies.get(position!!))
        }
    }


    fun addDataMovies(movies: MutableList<Movie>) {
        if (movies != null) {
            dataMovies.addAll(movies)
            notifyDataSetChanged()
        }
    }

    fun addDataLoading() {
        dataMovies.add(null)
        notifyDataSetChanged()
    }

    fun removeDataLoading() {
        dataMovies.removeAt(dataMovies.size - 1)
        notifyDataSetChanged()
    }

    fun getDateMovies(): Int {
        return this.dataMovies.size
    }

    override fun getItemCount(): Int = dataMovies.size

    override fun getItemViewType(position: Int): Int =
        if (dataMovies.get(position) != null) TYPE_MOVIE else TYPE_LOADING
}