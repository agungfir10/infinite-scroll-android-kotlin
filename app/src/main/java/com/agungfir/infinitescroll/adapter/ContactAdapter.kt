package com.agungfir.infinitescroll.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.agungfir.infinitescroll.R

class ContactAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var dataContacts: MutableList<String?> = mutableListOf()

    companion object {
        private const val TYPE_ITEM: Int = 1
        private const val TYPE_LOADING: Int = 2
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvName: TextView = itemView.findViewById(R.id.tvName)

        fun bind(name: String) {
            tvName.text = name
        }

    }

    class ProgressViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == TYPE_ITEM) {
            return ItemViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_contact, parent, false)
            )
        }
        return ProgressViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_loading, parent, false)
        )

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == TYPE_ITEM) {
            val itemViewHolder: ItemViewHolder = holder as ItemViewHolder
            itemViewHolder.bind(dataContacts.get(position)!!)
        }
    }


    fun addDataContacts(contacts: MutableList<String>) {
        if (contacts != null) {
            dataContacts.addAll(contacts)
            notifyDataSetChanged()
        }
    }

    fun addDataLoading() {
        dataContacts.add(null)
        notifyDataSetChanged()
    }

    fun removeDataLoading() {
        dataContacts.removeAt(dataContacts.size - 1)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = dataContacts.size

    override fun getItemViewType(position: Int): Int =
        if (dataContacts.get(position) != null) TYPE_ITEM else TYPE_LOADING
}