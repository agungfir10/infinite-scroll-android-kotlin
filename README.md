# Infinite Scroll

Jika tidak menggunakan infinite scroll akan berpengaruh ke source memori dan kuota pengguna, memuat satu halaman akan memakan waktu yang lama, memungkinkan seorang pengguna uninstall aplikasi tersebut

## Materi

- Logic Infinite Scroll
- Membuat Aplikasi Infinite Scroll tanpa API dan menggunakan API

## Pertanyaan

Hey, saya memiliki banyak data untuk ditampilkan kedalam sebuah aplikasi.
Apakah saya harus tampilkan semua data sekaligus atau tampilkan data sebagian-sebagian?

A. Tampilkan semua data sekaligus
B. Tampilkan data sebagian-sebagian

> Tampilkan data sebagian-sebagian, alasannya adalah dikarenakan data yang banyak. Jika kita ambil semua data sekaligus, maka yang terjadi adalah proses loading yang cukup lama dikarenakan halaman tersebut mengambil data yang banyak sekaligus dan akan berpengaruh ke source memori sampai dengan kuota pengguna

> Lalu untuk menampilkan data sebagian-sebagian bagaimana caranya?, kita bisa memiliki 2 cara yang sering digunakan yang pertama ada pagination dan yang kedua dengan teknis infinite scroll. Untuk di mobile biasanya kita menggunakan Infinite Scroll atau load more untuk menampilkan data tersebut seperti aplikasi Instagram, Twitter, Facebook, dsb.```

## Logic Infinite Scroll

![Infinite Scroll](images/logic-infinite-scroll.png)

## Pengembangan Lanjutan

- DetailMovie
- onSwipeRefresh data = 0
